import bpy
import re

bl_info = {
    "name": "Variants",
    "author": "Andy Goralczyk",
    "version": (1, 0),
    "blender": (2, 80, 0),
    "location": "Object context",
    "description": "Simple variants system",
    "warning": "",
    "doc_url": "",
    "category": "Workflow",
}


_variants_items = []


def strip_variant(coll_name):
    try:
        basename, variant = coll_name.rsplit('.', 1)
    except ValueError:
        return coll_name
    return basename


def strip_basename(coll_name):
    try:
        basename, variant = coll_name.rsplit('.', 1)
    except ValueError:
        return coll_name
    return variant


def direct_variants(basename: str):
    """Returns a list of collections which are only direct variants of basename"""
    direct_variants = []
    # Search only for basename + one word after the dot
    pattern = re.compile(r'^' + basename + r'\.\w+$')

    for coll in bpy.data.collections:
        match = re.search(pattern, coll.name)
        if match is None:
            continue
        direct_variants.append(bpy.data.collections[match.string])
    # Reverse the list to make 'low' always show up before 'high'
    direct_variants.reverse()
    return direct_variants


def set_variant(ob, coll_name):
    try:
        variant_collection = bpy.data.collections[coll_name]
    except KeyError:
        return False
    ob.variant.current = strip_basename(coll_name)
    ob.instance_collection = variant_collection
    return True


class Variant:
    @classmethod
    def poll(cls, context):
        ob = context.object
        return ob != None and ob.instance_type == 'COLLECTION'


class OBJECT_OT_GetVariants(bpy.types.Operator, Variant):
    """Look for variants of the active object in this file"""
    bl_idname = 'object.get_variants'
    bl_label = 'Get Variants'

    def execute(self, context):
        ob = context.object

        if not ob.instance_collection:
            self.report(
                {'ERROR'}, 'Specify a reference collection or reattach the asset')
            return {'CANCELLED'}

        ob.variant.basename = strip_variant(ob.instance_collection.name)
        ob.variant.current = strip_basename(ob.instance_collection.name)

        # Tidy up the name of the object
        ob.name = ob.name.replace(f'.{ob.variant.current}', "")

        if ob.instance_collection.library:
            ob.variant.instance_library = (
                ob.instance_collection.library.filepath)
        else:
            ob.variant.instance_library = 'LOCAL'

        ob.variant.available_variants.clear()
        for coll in direct_variants(ob.variant.basename):
            new_variant = ob.variant.available_variants.add()
            new_variant.name = strip_basename(coll.name)
            new_variant.collection = coll.name
        return {'FINISHED'}


class OBJECT_OT_SetVariant(bpy.types.Operator, Variant):
    """Set the variant of the active object from its available variants"""
    bl_idname = 'object.set_variant'
    bl_label = 'Set Variant'

    def variants_items(self, context):
        """callback of items for variant enum property"""
        ob = context.object

        _variants_items.clear()

        for item in ob.variant.available_variants:
            _variants_items.append((item.collection, item.name, item.name))
        return _variants_items

    variants: bpy.props.EnumProperty(
        items=variants_items,
        name='variants',
        description='Set the variant of the active object'
    )

    def execute(self, context):
        ob = context.object

        if set_variant(ob, self.variants):
            return {'FINISHED'}
        else:
            self.report(
                {'ERROR'}, 'Could not find variant collection in scene')
            return {'CANCELLED'}


class OBJECT_OT_SetVariantSelected(bpy.types.Operator, Variant):
    """Set the variant of the selected object(s) from available variants"""
    bl_idname = 'object.set_variant_selected'
    bl_label = 'Set variant on selected objects'

    def variants_items(self, context):
        """callback of items for variant enum property"""
        sel_objects = context.selected_objects

        _variants_items.clear()
        for ob in sel_objects:
            for item in ob.variant.available_variants:
                _variants_items.append((item.collection, item.name, item.name))
        return _variants_items

    variants: bpy.props.EnumProperty(
        items=variants_items,
        name='variants',
        description='Set the variant of the active object'
    )

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def execute(self, context):
        for ob in context.selected_objects:
            try:
                variant_collection = bpy.data.collections[self.variants]
            except KeyError:
                self.report(
                    {'ERROR'}, 'Could not find variant collection in scene')
                return {'CANCELLED'}

            ob.variant.current = strip_basename(self.variants)
            ob.instance_collection = variant_collection
        return {'FINISHED'}


class OBJECT_OT_SetVariantChild(bpy.types.Operator, Variant):
    """ """
    bl_idname = 'object.set_variant_child'
    bl_label = 'Set Variant Child'

    def execute(self, context):
        ob = context.object

        direct_var = direct_variants(
            ob.variant.basename + '.' + ob.variant.current)

        # Change active instanced collection to first occurence in child variants
        # ob.variant.current = strip_basename(direct_var[0].name)
        try:
            ob.instance_collection = direct_var[0]
            bpy.ops.object.get_variants()
            return {'FINISHED'}
        except IndexError:
            return {'CANCELLED'}


class OBJECT_OT_SetVariantParent(bpy.types.Operator, Variant):
    """ """
    bl_idname = 'object.set_variant_parent'
    bl_label = 'Set Variant Parent'

    def execute(self, context):
        ob = context.object
        try:
            ob.instance_collection = bpy.data.collections[ob.variant.basename]
            bpy.ops.object.get_variants()
            return {'FINISHED'}
        except KeyError:
            return {'CANCELLED'}


class OBJECT_OT_DetachAsset(bpy.types.Operator, Variant):
    """Detatch asset from instancer object, but keep linking information"""
    bl_idname = 'object.detach_asset'
    bl_label = 'Detach Asset'

    def execute(self, context):
        ob = context.object
        ob.instance_collection = None
        return {'FINISHED'}


class OBJECT_OT_ReattachAsset(bpy.types.Operator, Variant):
    """Attach asset from instancer object based on linking information"""
    bl_idname = 'object.reattach_asset'
    bl_label = 'Reattach Asset'

    def execute(self, context):
        ob = context.object
        coll_name = ob.variant.basename + '.' + ob.variant.current
        try:
            variant_collection = bpy.data.collections[coll_name]
        except KeyError:
            self.report(
                {'ERROR'}, 'Could not find variant collection in scene')

        ob.instance_collection = variant_collection
        return {'FINISHED'}


class VA_PT_switcher(bpy.types.Panel, Variant):
    bl_label = "Asset Variants"
    bl_idname = "OBJECT_PT_variants"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "object"

    def draw(self, context):
        ob = context.object

        layout = self.layout
        row = layout.row()

        row.operator('object.get_variants', icon='FILE_REFRESH')

        if ob.variant.available_variants and ob.instance_collection is not None:
            split = layout.split()
            split.label(text=f'Base name: {ob.variant.basename}')
            sub = split.row(align=True)
            sub.operator('object.set_variant_parent', text="", icon='BACK')
            sub.operator('object.set_variant_child', text="", icon='FORWARD')
            sub.operator_menu_enum(
                'object.set_variant', 'variants', text=ob.variant.current, icon='GROUP')

        row = layout.row()
        if ob.variant.basename:
            if ob.instance_collection:
                row.operator('object.detach_asset', icon='UNLINKED')
            else:
                row.operator('object.reattach_asset', icon='LINKED')


class VA_PT_variant_tools(bpy.types.Panel, Variant):
    bl_label = "Asset Toolbox"
    bl_idname = "OBJECT_PT_variant_tools"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'UI'
    bl_category = 'Asset'

    def draw(self, context):
        ob = context.object
        layout = self.layout
        layout.operator('object.set_variant_selected', text='Set Variant')
        # for ob in context.selected_objects:
        #     for var in ob.variant.available_variants:
        #         layout.label(text=var.name)


class VariantList(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(name="name")
    collection: bpy.props.StringProperty(name="collection")


class VariantProperties(bpy.types.PropertyGroup):
    basename: bpy.props.StringProperty(name='basename')
    instance_library: bpy.props.StringProperty(name='instance_library')
    current: bpy.props.StringProperty(name='current_variant')
    available_variants: bpy.props.CollectionProperty(type=VariantList)


classes = (
    OBJECT_OT_GetVariants,
    OBJECT_OT_SetVariant,
    OBJECT_OT_SetVariantSelected,
    OBJECT_OT_SetVariantParent,
    OBJECT_OT_SetVariantChild,
    OBJECT_OT_DetachAsset,
    OBJECT_OT_ReattachAsset,
    VA_PT_variant_tools,
    VA_PT_switcher,
    VariantList,
    VariantProperties,
)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    bpy.types.Object.variant = bpy.props.PointerProperty(
        type=VariantProperties)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)

    del bpy.types.Object.variant_properties


if __name__ == '__main__':
    register()
